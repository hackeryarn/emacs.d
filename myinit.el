(setq inhibit-splash-screen t
      inhibit-startup-echo-area-message t)
(setq backup-by-copying t)
(toggle-frame-fullscreen)
(transient-mark-mode 1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(show-paren-mode 1)
(defalias 'list-buffers 'ibuffer-other-window)
(fset 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "C-<f5>") 'revert-buffer)
(global-set-key (kbd "s-/") 'comment-or-uncomment-region)
(global-auto-revert-mode t)
(setq ispell-program-name "/usr/local/bin/ispell")
(eval-when-compile
  (require 'cl))

(setq backup-directory-alist '(("." . "~/emacs.d/backup"))
      backup-by-copyting t
      version-control t
      delete-old-versions t
      kept-new-versions 20
      kept-old-versions 5)

(require 're-builder)
(setq reb-re-syntax 'string)

(require 'dired-x)

(global-prettify-symbols-mode 1)

(defun add-pretty-symbols ()
  "makes some strings show as pretty Unicode symbols"
  (setq prettify-symbols-alist
        '(("lambda" . 955)
          ("<=" . 8804)
          (">=" . 8805)
          ("!=" . 8800)
          ("!==" . 8800)
          ("<>" . 8800)
          ("nil" . 8709)
          ("null" . 8709))))
(add-hook 'clojure-mode-hook 'add-pretty-symbols)
(add-hook 'enh-ruby-mode-hook 'add-pretty-symbols)
(add-hook 'js2-mode-hook 'add-pretty-symbols)
(add-hook 'web-mode-hook 'add-pretty-symbols)
(add-hook 'lisp-mode-hook 'add-pretty-symbols)

(fset 'frame-itemized
      [return ?\\ ?b ?e ?g ?i ?n ?\{ ?f ?r ?a ?m ?e ?\} return ?\\ ?b ?e ?g ?i ?n ?\{ ?i ?t ?e ?m ?i ?z ?e ?\} return return ?\\ ?e ?n ?d ?\{ ?i ?t ?e ?m ?i ?z ?e ?\} return ?\\ ?e ?n ?d ?  backspace ?\{ ?f ?r ?a ?m ?e ?\} return ?\C-p ?\C-p ?\C-p tab])

(fset 'frame-listing
   [?\\ ?b ?e ?g ?i ?n ?\{ ?f ?r ?a ?m ?e ?\} ?\[ ?f ?r ?a ?g ?i ?l ?e ?\] return ?\\ ?f ?r ?a ?m ?e ?t ?i ?t ?l ?e ?\{ ?\} return ?\\ ?b ?e ?g ?i ?n ?\{ ?l ?s ?t ?l ?i ?s ?t ?i ?n ?g ?\} return return ?\\ ?e ?n ?d ?\{ ?l ?s ?t ?l ?i ?s ?t ?i ?n ?g ?\} return ?\\ ?e ?n ?d ?\{ ?f ?r ?a ?m ?e ?\} tab ?\C-p ?\C-p ?\C-p ?\C-p right right right])

(use-package try
  :ensure t)

(use-package nlinum-relative
  :ensure t
  :config
  (global-nlinum-mode t)
  (global-nlinum-relative-mode t))

(use-package neotree
  :ensure t)

(use-package smex
  :ensure t)

(use-package expand-region
  :ensure t
  :bind (("C-=" . er/expand-region)))

(use-package diminish
  :ensure t
  :config
  (diminish 'abbrev-mode)
  (diminish 'auto-revert-mode))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.2)
  (setq which-key-echo-keystrokes 0))

(use-package org
  :ensure t
  :pin melpa
  :config
  (global-set-key "\C-cl" 'org-store-link)
  (global-set-key "\C-ca" 'org-agenda)
  (global-set-key "\C-cc" 'org-capture)
  (global-set-key "\C-cb" 'org-iswitchb)

  (setq org-todo-keywords '("TODO" "STARTED" "WAITING" "DONE"))

  (setq org-agenda-include-diary t)
  (setq org-agenda-include-all-todo t)
  (setq org-agenda-include-deadlines t)
  (org-indent-mode 1))

(use-package org-bullets
  :ensure t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((ruby . t)))

(setq org-capture-templates
   '(("o" "Organizer" entry
         (file+headline "~/Dropbox/org/organizer.org" "Inbox")
         "** TODO %?\n  %i\n")
        ("O" "Organizer with link" entry
         (file+headline "~/Dropbox/org/organizer.org" "Inbox")
         "** TODO %?\n  %i\n  %A\n")
        ("w" "Work" entry
         (file+headline "~/Dropbox/org/work.org" "Inbox")
         "** TODO %?\n  %i\n")
        ("W" "Work with link" entry
         (file+headline "~/Dropbox/org/work.org" "Inbox")
         "** TODO %?\n  %i\n  %A\n")
        ("j" "Journal" entry
         (file+datetree "~/Dropbox/org/journal.org")
         "* %?\n\nEntered on %U\n  %i\n")))

(use-package ox-reveal
  :ensure t
  :config
  (setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
  (setq org-reveal-mathjax t))

(use-package htmlize
  :ensure t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t)))

(defvar plantuml-path
  (expand-file-name (concat user-emacs-directory
                            (convert-standard-filename "ext/plantuml.jar"))))

(setq org-plantuml-jar-path plantuml-path)

(use-package plantuml-mode
  :ensure t
  :init
  (setq plantuml-jar-path plantuml-path)
  (add-to-list
   'org-src-lang-modes '("plantuml" . plantuml))
  (add-to-list
   'org-src-lang-modes '("ruby" . ruby)))

(use-package org-pomodoro
  :ensure t)

(use-package counsel
  :ensure t)

(use-package counsel-projectile
  :ensure t
  :bind ("C-c p f" . counsel-projectile))

(use-package swiper
  :init (ivy-mode 1)
  :diminish ivy-mode
  :ensure t
  :config
  ;; not sure if this needs to be in :init or :config
  (setq ivy-use-virtual-buffers t)
  (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
  :bind (("\C-s" . swiper)
         ("C-c C-r" . ivy-resume)
         ("<f6>" . ivy-resume)
         ("M-x" . counsel-M-x)
  	 ("M-i" . counsel-imenu)
         ("C-x C-f" . counsel-find-file)
         ("<f1> f" . counsel-describe-function)
         ("<f1> v" . counsel-describe-variable)
         ("<f1> l" . counsel-load-library)
         ("<f2> i" . counsel-info-lookup-symbol)
         ("<f2> u" . counsel-unicode-char)
         ("C-c g" . counsel-git):
         ("C-c j" . counsel-git-grep)
         ("C-c k" . counsel-ag)
         ("C-x l" . counsel-locate)
         ("C-S-o" . counsel-rhythmbox)
         ("M-y" . counsel-yank-pop)))

(use-package company
  :ensure t
  :diminish company-mode)

(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :init
  (yas-global-mode 1))

(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    ))

(setq aw-keys '(?a ?s ?d ?f ?g ?j ?k ?l))

(use-package paredit
  :ensure t
  :config
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t))

(use-package paredit-everywhere
  :ensure t)

(use-package rainbow-delimiters
  :ensure t)

(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :config
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-auto-closing t)
  (setq flycheck-check-syntax-automatically '(mode-enabled save))
  (setq flycheck-highlighting-mode 'columns))

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :config
  (projectile-global-mode))

(setq projectile-completion-system 'ivy)

(require 'erc)

(defvar erc-channels-work '(("freenode.net"
                             "#emacs"
                             "#ruby"
                             "#rubyonrails"
                             "#javascript"
                             "#jquery"
                             "#html5"
                             "#css")))

(defvar erc-channels-home '(("freenode.net"
                             "#emacs"
                             "#lisp"
                             "#clojure"
                             "#clojurescript")))

(defun erc-connect-freenode ()
  (erc :server "irc.freenode.net" :port "6667"
       :nick "__acher__"))

(defun erc-freenode-work ()
  (interactive)
  (setq erc-autojoin-channels-alist erc-channels-work)
  (erc-connect-freenode))

(defun erc-freenode-home ()
  (interactive)
  (setq erc-autojoin-channels-alist erc-channels-home)
  (erc-connect-freenode))

(global-set-key "\C-cew" 'erc-freenode-work)

(global-set-key "\C-ceh" 'erc-freenode-home)

(setq my-gmail-address "artemchernyak@gmail.com")

;;; Read mail

;; access email and offer to save password in encrypted file
(setq gnus-select-method `(nnimap "imap.gmail.com" (nnimap-user ,my-gmail-address)))
(setq auth-source '("~/.authinfo.gpg"))

;; show all folders, all emails and newest emails first
(add-hook 'ngus-startup-hook 'gnus-group-list-all-groups)
(setq gnus-parameters '((".*" (display . all))))
(setq gnus-thread-sort-function '((not gnus-thread-sort-by-number)))

;; show plain emails if possible, but offer buttons to choose
(setq mm-discouraged-alternatives '("text/html" "text/richtext"))
(setq gnus-inhibit-mime-unbutonizing t)
(setq gnus-buttonized-mime-type '("text/plain" "text/html" "text/richtext"))

(use-package magit
  :ensure t
  :bind (("C-x g" .  magit-status)
         ("C-x M-g" . magit-dispatch-popup)))

(use-package writegood-mode
  :ensure t)

(use-package slime
  :ensure t
  :config
  (require 'slime-autoloads)
  (setq inferior-lisp-program "/usr/local/bin/sbcl")
  (setq slime-contribs '(slime-fancy))
  (setq slime-lisp-implementations
        '((clisp ("/usr/local/bin/clisp"))
          (sbcl ("/usr/local/bin/sbcl")))))

(use-package cider
  :ensure t
  :config
  (setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))"))

(use-package clj-refactor
  :ensure t)

(use-package alchemist
  :ensure t)

(use-package enh-ruby-mode
  :ensure t
  :mode (("\\(Rake\\|Thor\\|Guard\\|Gem\\|Cap\\|Vagrant\\|Berks\\|Pod\\|Puppet\\)file\\'" . enh-ruby-mode)
         ("\\.\\(rb\\|rabl\\|ru\\|builder\\|rake\\|thor\\|gemspec\\|jbuilder\\)\\'" . enh-ruby-mode))
  :config
  (setq enh-ruby-deep-indent-paren nil))

(use-package projectile-rails
  :ensure t
  :diminish projectile-rails-mode)

(use-package rspec-mode
  :ensure t
  :config
  (setq rspec-use-spring-when-possible nil)
  (setq rspec-use-rvm t))

(use-package robe
  :ensure t
  :diminish robe-mode
  :config
  (eval-after-load 'company
    '(push 'company-robe company-backends))
  (defadvice inf-ruby-console-auto (before activate-rvm-for-robe activate)
    (rvm-activate-corresponding-ruby)))

(use-package rubocop
  :ensure t
  :diminish rubocop-mode)

(use-package rvm
  :ensure t
  :config
  (rvm-activate-corresponding-ruby))

(use-package web-mode
  :ensure t
  :mode (("\\.\\(.jsx\\|erb\\|html?\\|xml\\|css\\|scss\\|tsx\\)\\'" . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-auto-closing t)
  (setq web-mode-enable-css-colorization t)
  (setq web-mode-enable-sexp-functions t)
  (add-to-list 'web-mode-indentation-params '("case-extra-offset" . 2))
  (setq web-mode-content-types '(("jsx" . "\\.jsx?\\'")))
  (defadvice web-mode-highlight-part (around tweak-jsx activate)
    (if (or (equal web-mode-content-type "javascript")
            (equal web-mode-content-type "jsx"))
        (let ((web-mode-enable-part-fase nil))
          ad-do-it)
   ad-do-it)))

(use-package emmet-mode
  :ensure t
  :config
  (define-key emmet-mode-keymap (kbd "C-M-<right>") nil)
  (define-key emmet-mode-keymap (kbd "C-M-<left>") nil))

(use-package js2-mode
  :ensure t
  :mode (("\\.\\(js\\|es6\\)\\'" . js2-mode)
         ("\\.jsx\\'" . js2-jsx-mode))
  :config
  (setq js2-basic-offset 2))

(use-package jade-mode
  :ensure t)

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode t)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode t))

(use-package tide
    :ensure t
    :diminish tide-mode
    :config
    (setq company-tooltip-align-annotations t)
    (setq typescript-indent-level 2))

(diminish 'eldoc-mode)

(use-package yaml-mode
  :ensure t
  :mode ("\\.yml\\'" . yaml-mode))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package feature-mode
  :ensure t
  :mode (("\\.feature$\\'" . feature-mode)))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :bind (("C-M-f" . sp-forward-sexp)
         ("C-M-b" . sp-backward-sexp)

         ("C-M-d" . sp-down-sexp)
         ("C-M-a" . sp-backward-down-sexp)
         ("C-S-d" . sp-beginning-of-sexp)
         ("C-S-a" . sp-end-of-sexp)

         ("C-M-e" . sp-up-sexp)
         ("C-M-u" . sp-backward-up-sexp)
         ("C-M-t" . sp-transpose-sexp)

         ("C-M-n" . sp-next-sexp)
         ("C-M-p" . sp-previous-sexp)

         ("C-M-k" . sp-kill-sexp)
         ("C-M-w" . sp-copy-sexp)

         ("C-M-<right>" . sp-forward-slurp-sexp)
         ("C-M-<left>" . sp-forward-barf-sexp)
         ("C-S-<right>" . sp-backward-slurp-sexp)
         ("C-S-<left>" . sp-backward-barf-sexp))
  :config
  (require 'smartparens-config)
  (setq sp-highlight-pair-overlay nil)
  (setq sp-highlight-wrap-overlay nil)
  (setq sp-highlight-wrap-tag-overlay nil))

(defun iwb ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(defun get-buffers-matching-mode (mode)
  "Returns a list of buffers where their major-mode is equal to MODE"
  (let ((buffer-mode-matches '()))
    (dolist (buf (buffer-list))
    (with-current-buffer buf
        (if (eq mode major-mode)
            (add-to-list 'buffer-mode-matches buf))))
    buffer-mode-matches))

(defun sudo ()
  "Use TRAMP to 'sudo' the current buffer"
  (interactive)
  (when buffer-file-name
    (find-alternate-file
     (concat "/sudo:root@localhost:"
             buffer-file-name))))

(defun my-clojure-mode-hook ()
  (clj-refactor-mode 1)
  (cljr-add-keybindings-with-prefix "C-c C-m"))

(defun multi-occur-in-this-mode ()
  "Show all lines matching REGEXP in buffers with this major mode."
  (interactive)
  (multi-occur
   (get-buffers-matching-mode major-mode)
   (car (occur-read-primary-args))))

(global-set-key (kbd "C-<f2>") 'multi-occur-in-this-mode)

;; org-mode
(add-hook 'org-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; anter-init-hook
(add-hook 'after-init-hook 'global-company-mode)
(add-hook 'after-init-hook 'paredit-everywhere-mode)

;; paredit-mode
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enabnle-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'slime-mode-hook            #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'clojure-mode-hook          #'enable-paredit-mode)

;; rainbow-delimiters
(add-hook 'emacs-lisp-mode-hook       #'rainbow-delimiters-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'rainbow-delimiters-mode)
(add-hook 'ielm-mode-hook             #'rainbow-delimiters-mode)
(add-hook 'lisp-mode-hook             #'rainbow-delimiters-mode)
(add-hook 'lisp-interaction-mode-hook #'rainbow-delimiters-mode)
(add-hook 'slime-mode-hook            #'rainbow-delimiters-mode)
(add-hook 'scheme-mode-hook           #'rainbow-delimiters-mode)
(add-hook 'clojure-mode-hook          #'rainbow-delimiters-mode)

;; flycheck-mode
(add-hook 'elixir-mode-hook 'flycheck-mode)
(add-hook 'enh-ruby-mode-hook 'flycheck-mode)
(add-hook 'js2-mode-hook 'flycheck-mode)
(add-hook 'js2-jsx-mode-hook 'flycheck-mode)
(add-hook 'typescript-mode-hook 'flycheck-mode)
(add-hook 'web-mode-hook
          (lambda ()
            (when (or (equal web-mode-content-type "jsx")
		      (equal web-mode-content-type "javascript"))
	      (setq webm-ode-enable-auto-quoting nil)
	      (flycheck-mode))))

;; smart-parens
(add-hook 'elixir-mode-hook #'smartparens-mode)
(add-hook 'enh-ruby-mode-hook #'smartparens-mode)
(add-hook 'web-mode-hook #'smartparens-mode)
(add-hook 'js2-mode-hook #'smartparens-mode)

;; rubocop-mode
(add-hook 'enh-ruby-mode-hook 'rubocop-mode)

;; robe-mode
(add-hook 'enh-ruby-mode-hook 'robe-mode)
(add-hook 'enh-ruby-mode-hook 'robe-start)

;; projectile-rails
(add-hook 'projectile-mode-hook 'projectile-rails-on)

;; my-clojure-mode
(add-hook 'clojure-mode-hook #'my-clojure-mode-hook)

;; emmet-mode
(add-hook 'web-mode-hook 'emmet-mode)

;; js2-minor-mode
(add-hook 'js-mode-hook 'js2-minor-mode)

;; tide-mode
(add-hook 'web-mode-hook
          (lambda ()
            (when (string-equal "tsx" (file-name-extension buffer-file-name))
	      (setup-tide-mode))))
(add-hook 'typescript-mode-hook #'setup-tide-mode)
